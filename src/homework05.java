import java.util.Scanner;

public class homework05 {
    public static void main(String[]args) {

        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();

        int currentDigit = -1;
        int minDigit = Integer.MAX_VALUE;

        while (a != -1) {
            int current = a;
            while (current != 0) {
                currentDigit = current % 10;
                current = current / 10;

                if (currentDigit < minDigit) {
                    minDigit = currentDigit;
                }
            }
            a = scanner.nextInt();
        }
        System.out.println(minDigit);
    }
}